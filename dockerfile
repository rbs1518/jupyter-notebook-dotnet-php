# Copyright(c) Rinardi B. Sarean
# Install dotnet interactive and PHP to jupyter notebook docker image.
# Use 'jupyter/datascience-notebook' docker image as base.
# Use .NET Core 3.1 LTS and PHP 8.0.12

FROM jupyter/datascience-notebook:latest
ARG UBUNTU_VERSION=20.04

LABEL maintainer="Rinardi B. Sarean <rbs1518@gmail.com>"

USER root

ENV DOTNET_CLI_TELEMETRY_OPTOUT=1
ENV DOTNET_TRY_CLI_TELEMETRY_OPTOUT=1

RUN wget https://packages.microsoft.com/config/ubuntu/$UBUNTU_VERSION/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
    && dpkg -i packages-microsoft-prod.deb \
    && apt-get update --yes \
    && apt-get install --yes dotnet-sdk-3.1

RUN apt update \
    && apt install -y zip unzip git bzip2 \
    && apt install -y software-properties-common \
    && add-apt-repository ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y php8.0-cli php8.0-common php8.0-bz2 php8.0-zip php8.0-mbstring php8.0-mcrypt php8.0-zmq \
    && php -v

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && composer --version

RUN mkdir -p /home/${NB_USER}/.config/composer \
    && chmod -R 0777 /home/${NB_USER}/.config/composer

USER ${NB_UID}

RUN dotnet --version \
    && dotnet tool install --global Microsoft.dotnet-interactive --version 1.0.155302
ENV PATH="${PATH}:/home/${NB_USER}/.dotnet/tools"
RUN dotnet-interactive jupyter install

RUN composer --version \
    && composer global require rabrennie/jupyter-php-kernel
ENV PATH="${PATH}:/home/${NB_USER}/.config/composer/vendor/bin"
RUN jupyter-php-kernel --install